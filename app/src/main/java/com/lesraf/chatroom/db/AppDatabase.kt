package com.lesraf.chatroom.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.lesraf.chatroom.db.dao.ChatWithMessagesDao
import com.lesraf.chatroom.db.dao.ChatsDao
import com.lesraf.chatroom.db.dao.MessagesDao
import com.lesraf.chatroom.db.entity.pure.Chat
import com.lesraf.chatroom.db.entity.pure.Message

@Database(entities = [Chat::class, Message::class], version = 10)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun messageDao(): MessagesDao
    abstract fun chatDao(): ChatsDao
    abstract fun chatWithMessagesDao(): ChatWithMessagesDao

    companion object {
        fun provideDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "ChatRoomDatabase")
                    .allowMainThreadQueries() // Don't do this on a real app!
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}