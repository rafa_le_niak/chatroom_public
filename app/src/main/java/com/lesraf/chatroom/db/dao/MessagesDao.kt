package com.lesraf.chatroom.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.lesraf.chatroom.db.entity.pure.Message
import io.reactivex.Flowable

@Dao
interface MessagesDao {
    @Insert
    fun insert(message: Message)

    @Query("SELECT * FROM messages")
    fun getAll(): Flowable<List<Message>>

    @Query("SELECT * FROM messages WHERE chatId = :chatId")
    fun getByConversation(chatId: Long): LiveData<List<Message>>
}