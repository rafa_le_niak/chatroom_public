package com.lesraf.chatroom.db.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import com.lesraf.chatroom.db.entity.pure.Chat
import com.lesraf.chatroom.db.entity.pure.Message

class ChatWithMessages {
    @Embedded
    var chat: Chat? = null
    @Relation(parentColumn = "id", entityColumn = "chatId")
    var chatMessages: List<Message>? = null
}