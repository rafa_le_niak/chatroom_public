package com.lesraf.chatroom.db.entity.pure

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "messages")
@ForeignKey(parentColumns = ["id"], childColumns = ["chatId"], entity = Chat::class)
class Message(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        val chatId: Long,
        @ColumnInfo(name = "fromWho") val from: String,
        val message: String,
        val date: Date = Date()
)