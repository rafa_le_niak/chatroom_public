package com.lesraf.chatroom.db.entity.pure

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "chats")
data class Chat(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        val name: String,
        @Embedded(prefix = "message_") val lastMessage: Message?
)