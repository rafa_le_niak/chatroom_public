package com.lesraf.chatroom.db.dao

import android.arch.persistence.room.*
import com.lesraf.chatroom.db.entity.pure.Chat
import com.lesraf.chatroom.db.entity.ChatForMainList
import com.lesraf.chatroom.db.entity.pure.Message

@Dao
abstract class ChatsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(chat: Chat)

    @Query("SELECT * FROM chats")
    abstract fun getAll(): List<Chat>

    @Query("SELECT id, name, message_message FROM chats")
    abstract fun getAllForMainList(): List<ChatForMainList>

    @Query("SELECT * FROM chats WHERE id = :id")
    abstract fun getById(id: Long): Chat

    @Transaction // all these operations will happen in one transaction
    open fun updateLastMessage(message: Message) {
        val chat = getById(message.chatId)
        val chatWithUpdatedLastMessage = chat.copy(lastMessage = message)
        insert(chatWithUpdatedLastMessage)
    }

    @Query("DELETE FROM chats WHERE id = :id")
    abstract fun delete(id: Long)
}