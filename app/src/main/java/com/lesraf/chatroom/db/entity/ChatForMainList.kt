package com.lesraf.chatroom.db.entity

import android.arch.persistence.room.ColumnInfo

data class ChatForMainList(
        val id: Long,
        val name: String,
        @ColumnInfo(name = "message_message")
        val lastMessage: String?
)