package com.lesraf.chatroom.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.lesraf.chatroom.db.entity.ChatWithMessages

@Dao
interface ChatWithMessagesDao {
    @Query("SELECT * FROM chats")
    fun getAll(): List<ChatWithMessages>
}