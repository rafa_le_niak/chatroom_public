package com.lesraf.chatroom

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import com.lesraf.chatroom.db.entity.pure.Message
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity() {

    private var conversationId: Long = -1L

    private val messageDao = Injection.database.messageDao()
    private val chatDao = Injection.database.chatDao()
    private val database = Injection.database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        titleTextView.text = intent.getStringExtra(NAME_EXTRA)
        conversationId = intent.getLongExtra(ID_EXTRA, -1L)

        sendMessageButton.setOnClickListener {
            onSendMessageClick()
        }
    }

    private fun onSendMessageClick() {
        database.runInTransaction {

            val message = Message(
                    chatId = conversationId,
                    from = nickEditText.text.toString(),
                    message = messageEditText.text.toString()
            )

            messageDao.insert(message)
            chatDao.updateLastMessage(message)
        }

        messageEditText.setText("")
    }

    override fun onResume() {
        super.onResume()
        showMessages()
    }

    private fun showMessages() {
        messageDao.getByConversation(conversationId)
                .observe(this, Observer { messages -> inflateMessages(messages) })
    }

    private fun inflateMessages(messages: List<Message>?) {
        messagesLinearLayout.removeAllViews()
        messages?.forEach { message ->
            with(TextView(this)) {
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
                text = "${message.date}\n${message.from}: ${message.message}\n"

                messagesLinearLayout.addView(this)
            }
        }

        messagesLinearLayout.post { messagesScrollView.fullScroll(View.FOCUS_DOWN) }
    }

    companion object {
        private const val NAME_EXTRA = "NAME_EXTRA"
        private const val ID_EXTRA = "ID_EXTRA"

        fun intent(context: Context, id: Long, name: String): Intent {
            val intent = Intent(context, ChatActivity::class.java)

            intent.putExtra(ID_EXTRA, id)
            intent.putExtra(NAME_EXTRA, name)

            return intent
        }
    }
}
