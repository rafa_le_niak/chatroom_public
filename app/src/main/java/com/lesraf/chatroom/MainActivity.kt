package com.lesraf.chatroom

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.lesraf.chatroom.db.entity.pure.Chat
import com.lesraf.chatroom.db.entity.ChatForMainList
import com.lesraf.chatroom.db.entity.ChatWithMessages
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val chatModel = Injection.database.chatDao()
    private val chatWithMessagesModel = Injection.database.chatWithMessagesDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val chatModel = Injection.database.chatDao()

        addNewChatButton.setOnClickListener {
            chatModel.insert(Chat(name = newChatEditText.text.toString(), lastMessage = null))
            newChatEditText.setText("")

            showChats()
        }
    }

    override fun onResume() {
        super.onResume()

        showChats()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_statistics) {
            logStatistics()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun inflateConversations(chats: List<ChatForMainList>) {
        linearLayout.removeAllViews()
        chats.forEach { chat ->
            with(TextView(this)) {
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)
                text = "${chat.name}: ${chat.lastMessage}"

                setOnClickListener {
                    startActivity(ChatActivity.intent(this@MainActivity, chat.id, chat.name))
                }

                setOnLongClickListener {
                    deleteChat(chat.id)
                }

                linearLayout.addView(this)
            }

        }
        linearLayout.post { chatsScrollView.fullScroll(View.FOCUS_DOWN) }
    }


    private fun logStatistics() {
        val chats: List<ChatWithMessages> = chatWithMessagesModel.getAll()

        val stringBuilder = StringBuilder()

        stringBuilder.append("<Name>: <Messages count>\n")
        for (chat in chats) {
            stringBuilder.append("${chat.chat?.name}: ${chat.chatMessages?.size}\n")
        }

        Log.d("Statistics", stringBuilder.toString())
    }

    private fun showChats() {
        inflateConversations(chatModel.getAllForMainList())
    }

    private fun deleteChat(chat: Long): Boolean {
        chatModel.delete(chat)
        showChats()
        return true
    }
}
