package com.lesraf.chatroom

import android.app.Application
import com.facebook.stetho.Stetho
import com.lesraf.chatroom.db.entity.pure.Chat
import com.lesraf.chatroom.db.entity.pure.Message

class ChatRoomApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this)

        Injection.initialize(applicationContext)

//        mockData()
    }

    private fun mockData() {
        val messageModel = Injection.database.messageDao()
        val chatModel = Injection.database.chatDao()

        //prepare messages
        val chatOpenSpam = Chat(
                name = "Open Spam",
                lastMessage = null
        )
        val message = Message(
                chatId = chatOpenSpam.id,
                from = "Rafal",
                message = "Hello, it's me."
        )
        val openSpamWithLastMessage = chatOpenSpam.copy(lastMessage = message)

        chatModel.insert(openSpamWithLastMessage)
        chatModel.insert(Chat(name = "android-dev", lastMessage = null))

        messageModel.insert(message)
        messageModel.insert(Message(
                chatId = chatOpenSpam.id,
                from = "Ktos",
                message = "Nie."
        ))
    }
}