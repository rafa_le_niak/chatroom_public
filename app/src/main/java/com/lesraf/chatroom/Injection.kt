package com.lesraf.chatroom

import android.content.Context
import com.lesraf.chatroom.db.AppDatabase

object Injection {

    lateinit var database: AppDatabase

    fun initialize(context: Context) {
        database = AppDatabase.provideDatabase(context)
    }
}